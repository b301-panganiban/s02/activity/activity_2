package com.zuitt.example;

import java.util.ArrayList;
import java.util.HashMap;

public class WDC043_S2_A2 {
    public static void main(String[] args) {

        //Array
        int[] intArray = new int[5];
        intArray[0] = 2;
        intArray[1] = 3;
        intArray[2] = 5;
        intArray[3] = 7;
        intArray[4] = 11;

        System.out.println("The first prime number is: " + intArray[0]);
        System.out.println("The second prime number is: " + intArray[1]);
        System.out.println("The third prime number is: " + intArray[2]);
        System.out.println("The fourth prime number is: " + intArray[3]);
        System.out.println("The fifth prime number is: " + intArray[4]);

        //ArrayList
        ArrayList<String> friends = new ArrayList<String>();

        friends.add("John");
        friends.add("Jane");
        friends.add("Chloe");
        friends.add("Zoey");

        System.out.println("My friends are: " + friends);

        //HashMaps
        HashMap<String, Integer> inventory = new HashMap<String, Integer>();

        inventory.put("Toothpaste", 15);
        inventory.put("Toothbrush", 20);
        inventory.put("Soap", 12);

        System.out.println("Our current inventory consists of: " + inventory);
    }
}
